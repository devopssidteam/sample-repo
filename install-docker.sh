#!/bin/bash

# Install Docker CE
## Set up the repository
### Install required packages.
 apt-get remove docker docker-engine docker.io containerd runc
 apt-get update -y
 apt-get -y install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

### Add Docker repository.
curl -fsSL https://download.docker.com/linux/ubuntu/gpg |  gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" |  tee /etc/apt/sources.list.d/docker.list > /dev/null

## Install Docker CE.
 apt-get update -y && \
 apt-get -y install docker-ce=5:18.09.0~3-0~ubuntu-bionic docker-ce-cli=5:18.09.0~3-0~ubuntu-bionic containerd.io

## Create /etc/docker directory.
 mkdir -p /etc/docker

# Setup daemon.
 cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2",
  "storage-opts": [
    "overlay2.override_kernel_check=true"
  ]
}
EOF

 mkdir -p /etc/systemd/system/docker.service.d

 systemctl enable docker

# Restart Docker
 systemctl daemon-reload
 systemctl restart docker